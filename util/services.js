import { sleep } from 'k6';
import http from 'k6/http';

let BASE_URL = 'https://api.migracion.tk';

if (__ENV.ENV === 'local') {
    BASE_URL = 'http://localhost:8000';
}

const SLEEP_DURATION = 0.1;

export function status() {
    let response = http.get(BASE_URL)
    sleep(SLEEP_DURATION);

    return response;
}
