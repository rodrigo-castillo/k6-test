import { status } from './util/services.js'
import { describe } from 'https://jslib.k6.io/expect/0.0.4/index.js';
import { check, fail } from 'k6';
import exec from 'k6/execution';

const executor = 'constant-arrival-rate';
// const executor = 'ramping-vus';

// ============================================================
// ==============      constant-arrival-rate     ==============
// ============================================================
const ratePerSecond = 500;
// const rate = Math.ceil(ratePerSecond / 1);
const rate = ratePerSecond;
const maxVUs = 500;
const preAllocatedVUs = 20;
const duration = '20s';
// ============================================================
// ============================================================

// .   .   .   .   .   .   .   .   .   .   .   .   .   .   .

// ============================================================
// =================        ramping-vus       =================
// ============================================================
const totalUvs = 20;
const stagesDuration = ['5s', '5m', '5s'];
// ============================================================
// ============================================================

const target = Math.ceil(totalUvs);

export const options = {
    thresholds: {
        http_req_failed: ['rate<0.20'],
    },

    // Puede decirle a k6 que no procese el cuerpo de la respuesta
    // discardResponseBodies: true,

    scenarios: {
        'constant-arrival-rate': {
            constant_request_rate: {
                executor: 'constant-arrival-rate',
                rate,
                timeUnit: '1s',
                duration,
                preAllocatedVUs,
                maxVUs,
            }
        },
        'ramping-vus': {
            new_customer: {
                executor: 'ramping-vus',
                startVUs: 0,
                stages: [
                    { duration: stagesDuration[0], target },
                    { duration: stagesDuration[1], target },
                    { duration: stagesDuration[2], target: 0 },
                ],
                gracefulRampDown: '30s',
            },
        }
    }[executor],
};


export function setup() {
    //
}

export default function () {
    const iteration = exec.scenario.iterationInTest;

    describe('Status', () => {
        let responseStatus = status();

        // if (!check(responseStatus, { 'status': (r) => r.status === 200 }) || iteration % 13 === 0) {
        if (!check(responseStatus, { 'status': (r) => r.status === 200 })) {
            fail(`status status:${responseStatus.status} error:${responseStatus.error}`);
        }
    });
}

export function teardown() {
    //
}

